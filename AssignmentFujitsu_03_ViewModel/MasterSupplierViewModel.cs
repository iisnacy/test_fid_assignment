﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentFujitsu_03_ViewModel
{
    public class MasterSupplierViewModel
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string ADDRESS { get; set; }
        public string PROVINCE { get; set; }
        public string CITY { get; set; }
        public string PIC { get; set; }
    }
}
