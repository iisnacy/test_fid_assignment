﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentFujitsu_03_ViewModel
{
    class TransaksiOrderViewModel
    {
        public string ORDER_NO { get; set; }
        public Nullable<System.DateTime> ORDER_DATE { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public Nullable<decimal> AMOUNT { get; set; }
    }
}
