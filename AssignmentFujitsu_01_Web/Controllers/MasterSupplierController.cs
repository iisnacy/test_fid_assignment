﻿using AssignmentFujitsu_01_Web;
using AssignmentFujitsu_02_DataAccess;
using AssignmentFujitsu_03_ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssignmentFujitsu_01_Web.Controllers
{
    public class MasterSupplierController : Controller
    {
        // GET: MasterSupplier
        public ActionResult Index()
        {
            ViewBag.PROVINCE = new SelectList(MasterSupplierDataAccess.GetListAll(),"SUPPLIER_CODE", "PROVINCE");
            ViewBag.CITY = new SelectList(MasterSupplierDataAccess.GetListAll(),"SUPPLIER_CODE", "CITY");
            return View();
        }


        public ActionResult Search(string paramSearch)
        {
            int countDataDb = MasterSupplierDataAccess.GetCountData(paramSearch);

            return PartialView("List", MasterSupplierDataAccess.GetListAll(paramSearch.ToLower()));
        }

        public ActionResult Create()
        {
            return PartialView();
        }

       
    }
}