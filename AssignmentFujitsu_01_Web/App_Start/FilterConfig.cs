﻿using System.Web;
using System.Web.Mvc;

namespace AssignmentFujitsu_01_Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
