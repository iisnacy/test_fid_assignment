﻿using AssignmentFujitsu_03_ViewModel;
using AssignmentFujitsu_04_DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssignmentFujitsu_02_DataAccess
{
    public class MasterSupplierDataAccess
    {
        public static string Message = string.Empty;

        public static List<MasterSupplierViewModel> GetListAll()
        {
            List<MasterSupplierViewModel> result = new List<MasterSupplierViewModel>();
            using (var db = new TEST_FIDEntities())
            {
                result = (from a in db.TB_M_SUPPLIER
                          select new MasterSupplierViewModel
                          {
                              SUPPLIER_CODE = a.SUPPLIER_CODE,
                              SUPPLIER_NAME = a.SUPPLIER_NAME,
                              ADDRESS = a.ADDRESS,
                              PROVINCE = a.PROVINCE,
                              CITY = a.CITY,
                              PIC = a.PIC
                          }).ToList();
            }
            return result;
        }

        public static List<MasterSupplierViewModel> GetListAll(string paramSearch)
        {
            List<MasterSupplierViewModel> result = new List<MasterSupplierViewModel>();
            using (var db = new TEST_FIDEntities())
            {
                IQueryable<MasterSupplierViewModel> query;
                {
                    query = (from a in db.TB_M_SUPPLIER
                             where a.SUPPLIER_CODE.ToLower().Contains(paramSearch)
                             || a.PROVINCE.ToLower().Contains(paramSearch)
                             select new MasterSupplierViewModel
                             {
                                 SUPPLIER_CODE = a.SUPPLIER_CODE,
                                 SUPPLIER_NAME = a.SUPPLIER_NAME,
                                 ADDRESS = a.ADDRESS,
                                 PROVINCE = a.PROVINCE,
                                 CITY = a.CITY,
                                 PIC = a.PIC
                             });
                }
                return query.ToList();
            }
        }

        public static int GetCountData(string paramSearch)
        {
            int countData = 0;

            using (var db = new TEST_FIDEntities())
            {
                countData = (db.TB_M_SUPPLIER.Count(
                    a => a.SUPPLIER_CODE.ToLower().Contains(paramSearch) ||
                    a.PROVINCE.ToLower().Contains(paramSearch)
                    ));
            }
            return countData;
        }
    }

    //public static MasterSupplierViewModel GetDetailsByCode(string paramCode)
    //{
    //    MasterSupplierViewModel result = new MasterSupplierViewModel();
    //    using (var db = new TEST_FIDEntities())
    //    {
    //        result = (from a in db.TB_M_SUPPLIER
    //                  where a.SUPPLIER_CODE == paramCode
    //                  select new MasterSupplierViewModel
    //                  {
    //                      SUPPLIER_CODE = a.SUPPLIER_CODE,
    //                      SUPPLIER_NAME = a.SUPPLIER_NAME,
    //                      ADDRESS = a.ADDRESS,
    //                      PROVINCE = a.PROVINCE,
    //                      CITY = a.CITY,
    //                      PIC = a.PIC
    //                  }).FirstOrDefault();
    //    }
    //    return result;
    //}


    public static bool Insert(MasterSupplierViewModel paramModel)
    {
        bool result = true;
        try
        {
            using (var db = new TEST_FIDEntities())
            {
                TB_M_SUPPLIER a = new TB_M_SUPPLIER();
                a.SUPPLIER_CODE = paramModel.SUPPLIER_CODE;
                a.SUPPLIER_NAME = paramModel.SUPPLIER_NAME;
                a.ADDRESS = paramModel.ADDRESS;
                a.PROVINCE = paramModel.PROVINCE;
                a.CITY = paramModel.CITY;
                a.PIC = paramModel.PIC;
                db.TB_M_SUPPLIER.Add(a);
                db.SaveChanges();
            }
            Message = "Data berhasil disimpan!";
        }
        catch (Exception hasError)
        {
            result = false;
            if (hasError.Message.ToLower().Contains("inner exception"))
            {
                Message = hasError.InnerException.InnerException.Message;
            }
            else
            {
                Message = hasError.Message;
            }
        }
        return result;
    }


}
